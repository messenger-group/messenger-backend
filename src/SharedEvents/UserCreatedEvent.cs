﻿namespace SharedEvents;

public record UserCreatedEvent(
    Guid Id,
    string Name,
    string Surname,
    string? Patronymic,
    string Email);