﻿namespace SharedEvents;

public record SendEmailNotification(string Email, string Title, string Text);