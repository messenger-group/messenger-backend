﻿using UsersDomain.Entities;

namespace UsersApplication;

public interface IUserRepository
{
    IAsyncEnumerable<User> GetUsers();

    void AddUser(User user);
    Task<bool> ExistsUserWithSameEmail(string email, CancellationToken cancellationToken = default);

    Task SaveChanges(CancellationToken cancellationToken = default);
}