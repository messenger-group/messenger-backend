﻿using SharedEvents;

namespace UsersApplication;

public interface INotificationBus
{
    Task NotifyUserCreated(UserCreatedEvent userCreatedEvent);
}