﻿namespace UsersApplication.Exceptions;

public class EmailAlreadyExistsException(string email) : Exception($"Пользователь {email} уже существует");