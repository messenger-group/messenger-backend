﻿namespace UsersApplication.Dto;

public record UserDto(
    Guid Id,
    string Name,
    string Surname,
    string? Patronymic,
    string Email);