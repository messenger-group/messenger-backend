﻿using SharedEvents;
using UsersApplication.Dto;
using UsersApplication.Exceptions;
using UsersDomain.Entities;
using UsersDomain.ValueObjects;

namespace UsersApplication.UseCases;

public record CreateUser(
    string Name,
    string Surname,
    string? Patronymic,
    string Email);

public class CreateUserHandler
{
    private readonly IUserRepository _userRepository;
    private readonly INotificationBus _notificationBus;

    public CreateUserHandler(IUserRepository userRepository, INotificationBus notificationBus)
    {
        _userRepository = userRepository;
        _notificationBus = notificationBus;
    }

    public async Task<UserDto> Handle(CreateUser command, CancellationToken cancellationToken)
    {
        var email = command.Email;
        if (await _userRepository.ExistsUserWithSameEmail(email, cancellationToken))
        {
            throw new EmailAlreadyExistsException(email);
        }

        var name = command.Name;
        var surname = command.Surname;
        var patronymic = command.Patronymic;

        var user = new User
        {
            Id = Guid.NewGuid(),
            Fio = new Fio(name, surname, patronymic),
            Email = email
        };

        _userRepository.AddUser(user);

        await _notificationBus.NotifyUserCreated(new UserCreatedEvent(
            user.Id,
            name,
            surname,
            patronymic,
            email));

        await _userRepository.SaveChanges(cancellationToken);

        return new UserDto(user.Id, name, surname, patronymic, email);
    }
}