﻿using System.Runtime.CompilerServices;
using UsersApplication.Dto;

namespace UsersApplication.UseCases;

public record GetUsersList;

public class GetUsersListHandler
{
    private readonly IUserRepository _userRepository;

    public GetUsersListHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async IAsyncEnumerable<UserDto> Handle(
        GetUsersList _,
        [EnumeratorCancellation] CancellationToken cancellationToken)
    {
        await foreach (var user in _userRepository.GetUsers().WithCancellation(cancellationToken))
        {
            yield return new UserDto(user.Id, user.Fio.Name, user.Fio.Surname, user.Fio.Patronymic, user.Email);
        }
    }
}