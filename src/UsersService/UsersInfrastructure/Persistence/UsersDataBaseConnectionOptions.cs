﻿using DataBindingsExtensions;

namespace UsersInfrastructure.Persistence;

internal class UsersDataBaseConnectionOptions
{
    [EnvironmentVariableName("DB_HOST")]
    public string Host { get; init; } = string.Empty;

    [EnvironmentVariableName("DB_PORT")]
    public short Port { get; init; }

    [EnvironmentVariableName("DB_NAME")]
    public string DataBase { get; init; } = string.Empty;

    [EnvironmentVariableName("DB_USER")]
    public string User { get; init; } = string.Empty;

    [EnvironmentVariableName("DB_PASSWORD")]
    public string Password { get; init; } = string.Empty;

    public sealed override string ToString()
    {
        return $"Host={Host};Port={Port};Database={DataBase};Username={User};Password={Password}";
    }
}