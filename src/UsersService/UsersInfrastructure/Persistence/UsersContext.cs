﻿using MassTransit;
using Microsoft.EntityFrameworkCore;
using UsersDomain.Entities;
using UsersInfrastructure.Persistence.Configurations;

namespace UsersInfrastructure.Persistence;

internal class UsersContext : DbContext
{
    public DbSet<User> Users => Set<User>();

    public UsersContext(DbContextOptions<UsersContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ApplyConfiguration(new UserConfiguration());

        modelBuilder.AddInboxStateEntity();
        modelBuilder.AddOutboxMessageEntity();
        modelBuilder.AddOutboxStateEntity();
    }
}