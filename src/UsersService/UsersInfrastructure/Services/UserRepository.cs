﻿using Microsoft.EntityFrameworkCore;
using UsersApplication;
using UsersDomain.Entities;
using UsersInfrastructure.Persistence;

namespace UsersInfrastructure.Services;

internal class UserRepository : IUserRepository
{
    private readonly UsersContext _usersContext;

    public UserRepository(UsersContext usersContext)
    {
        _usersContext = usersContext;
    }

    public IAsyncEnumerable<User> GetUsers()
    {
        return _usersContext
            .Users
            .AsAsyncEnumerable();
    }

    public void AddUser(User user)
    {
        _usersContext
            .Users
            .Add(user);
    }

    public Task<bool> ExistsUserWithSameEmail(string email, CancellationToken cancellationToken = default)
    {
        return _usersContext
            .Users
            .AnyAsync(x => x.Email == email, cancellationToken);
    }

    public async Task SaveChanges(CancellationToken cancellationToken = default)
    {
        await _usersContext.SaveChangesAsync(cancellationToken);
    }
}