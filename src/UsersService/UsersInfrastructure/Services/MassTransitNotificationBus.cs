﻿using MassTransit;
using SharedEvents;
using UsersApplication;

namespace UsersInfrastructure.Services;

internal class MassTransitNotificationBus : INotificationBus
{
    private readonly IPublishEndpoint _publishEndpoint;

    public MassTransitNotificationBus(IPublishEndpoint publishEndpoint)
    {
        _publishEndpoint = publishEndpoint;
    }

    public Task NotifyUserCreated(UserCreatedEvent userCreatedEvent)
    {
        return _publishEndpoint.Publish(userCreatedEvent);
    }
}