﻿using DataBindingsExtensions;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using UsersApplication;
using UsersApplication.UseCases;
using UsersInfrastructure.Persistence;
using UsersInfrastructure.Services;

namespace UsersInfrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services)
    {
        return services
            .AddDataBase()
            .AddMasstransit()
            .AddScoped<IUserRepository, UserRepository>()
            .AddScoped<INotificationBus, MassTransitNotificationBus>()
            .AddScoped<GetUsersListHandler>()
            .AddScoped<CreateUserHandler>();
    }

    public static async Task InitDb(this IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();

        var context = scope.ServiceProvider.GetRequiredService<UsersContext>();

        await context.Database.EnsureCreatedAsync();
    }

    private static IServiceCollection AddMasstransit(this IServiceCollection services)
    {
        var connectionOptions = GetConnectionOptions();

        return services
            .AddMassTransit(options =>
            {
                options.SetKebabCaseEndpointNameFormatter();

                options.AddEntityFrameworkOutbox<UsersContext>(o =>
                {
                    o.UsePostgres().UseBusOutbox();
                    o.QueryDelay = TimeSpan.FromSeconds(30);
                });

                options.UsingRabbitMq((context, config) =>
                {
                    config.Host(connectionOptions.Host, "/", h =>
                    {
                        h.Username(connectionOptions.UserName);
                        h.Password(connectionOptions.Password);
                    });

                    config.ConfigureEndpoints(context);
                });
            });

        static RabbitMqConnectionOptions GetConnectionOptions()
        {
            return EnvironmentLoader.Load<RabbitMqConnectionOptions>() ??
                   throw new Exception("RabbitMq options not found");
        }
    }

    private static IServiceCollection AddDataBase(this IServiceCollection services)
    {
        var connectionString = GetConnectionString();

        services.AddDbContext<UsersContext>(options => options.UseNpgsql(connectionString));

        return services;

        static string GetConnectionString()
        {
            return EnvironmentLoader.Load<UsersDataBaseConnectionOptions>()?.ToString() ?? string.Empty;
        }
    }
}