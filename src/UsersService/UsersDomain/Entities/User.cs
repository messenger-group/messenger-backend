﻿using UsersDomain.ValueObjects;

namespace UsersDomain.Entities;

public class User
{
    public required Guid Id { get; init; }
    public required Fio Fio { get; init; }

    public required string Email { get; init; }
}