﻿namespace UsersDomain.ValueObjects;

public record Fio(string Name, string Surname, string? Patronymic);