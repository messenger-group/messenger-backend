﻿using Microsoft.AspNetCore.Mvc;
using UsersApplication.Exceptions;

namespace UsersWebApi.Middlewares;

public class ExceptionHandlingMiddleware
{
    private readonly RequestDelegate _next;

    public ExceptionHandlingMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (EmailAlreadyExistsException exception)
        {
            var problemDetails = new ProblemDetails
            {
                Detail = exception.Message
            };

            await Results.BadRequest(problemDetails).ExecuteAsync(context);
        }
        catch (Exception)
        {
            await Results.Problem().ExecuteAsync(context);
        }
    }
}