﻿using Microsoft.AspNetCore.Mvc;
using UsersApplication.UseCases;

namespace UsersWebApi;

public static class UsersEndpoints
{
    public static void MapUserEndpoints(this WebApplication app)
    {
        var group = app
            .MapGroup("api/users")
            .WithOpenApi()
            .WithTags("Users");

        group.MapGet(string.Empty, (
            [FromServices] GetUsersListHandler handler,
            CancellationToken cancellationToken) => handler.Handle(new GetUsersList(), cancellationToken));

        group.MapPost(string.Empty, (
            [FromBody] CreateUser command,
            [FromServices] CreateUserHandler handler,
            CancellationToken cancellationToken) => handler.Handle(command, cancellationToken));
    }
}