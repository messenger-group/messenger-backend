using UsersInfrastructure;
using UsersWebApi;
using UsersWebApi.Middlewares;

var builder = WebApplication.CreateBuilder(args);

var services = builder.Services;

services
    .AddEndpointsApiExplorer()
    .AddSwaggerGen()
    .AddInfrastructure();

var app = builder.Build();

await app.Services.InitDb();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ExceptionHandlingMiddleware>();

app.UseHttpsRedirection();

app.MapUserEndpoints();

app.Run();
