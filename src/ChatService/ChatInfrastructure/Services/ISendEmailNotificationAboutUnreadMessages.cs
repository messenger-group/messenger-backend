﻿namespace ChatInfrastructure.Services;

public interface ISendEmailNotificationAboutUnreadMessages
{
    Task SendNotification(CancellationToken cancellationToken = default);
}