﻿using ChatApplication;
using ChatDomain.Entities;
using ChatInfrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace ChatInfrastructure.Services;

internal class UsersRepository : IUsersRepository
{
    private readonly ChatContext _chatContext;

    public UsersRepository(ChatContext chatContext)
    {
        _chatContext = chatContext;
    }

    public Task<List<User>> GetUsers(IReadOnlyCollection<Guid> userIds, CancellationToken cancellationToken = default)
    {
        return _chatContext
            .Users
            .Where(x => userIds.Contains(x.Id))
            .ToListAsync(cancellationToken);
    }
}