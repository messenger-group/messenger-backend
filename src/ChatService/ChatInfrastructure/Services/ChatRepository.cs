﻿using ChatApplication;
using ChatDomain;
using ChatDomain.Entities;
using ChatInfrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace ChatInfrastructure.Services;

internal class ChatRepository : IChatRepository
{
    private readonly ChatContext _chatContext;

    public ChatRepository(ChatContext chatContext)
    {
        _chatContext = chatContext;
    }

    public IAsyncEnumerable<Chat> GetUserChats(Guid userId)
    {
        return _chatContext
            .Chats
            .Where(x => x.Members.Any(m => m.UserId == userId))
            .AsAsyncEnumerable();
    }

    public Task<Chat?> GetById(Guid chatId, CancellationToken cancellationToken = default)
    {
        return _chatContext
            .Chats
            .SingleOrDefaultAsync(x => x.Id == chatId, cancellationToken);
    }

    public async Task<Chat?> TryGetExistingPrivateChat(
        IReadOnlyCollection<Guid> userIds,
        CancellationToken cancellationToken = default)
    {
        if (userIds.Count != 2)
        {
            return null;
        }

        var firstId = userIds.ElementAt(0);
        var secondId = userIds.ElementAt(1);

        return await _chatContext
            .Chats
            .Where(x => x.Type == ChatType.Private)
            .Where(x => x.Members.Any(m => m.UserId == firstId)
                        && x.Members.Any(m => m.UserId == secondId))
            .SingleOrDefaultAsync(cancellationToken);
    }

    public async Task SaveChat(Chat chat, CancellationToken cancellationToken = default)
    {
        _chatContext
            .Chats
            .Add(chat);

        await _chatContext.SaveChangesAsync(cancellationToken);
    }

    public async Task UpdateChat(Chat chat, CancellationToken cancellationToken = default)
    {
        await _chatContext.SaveChangesAsync(cancellationToken);
    }
}