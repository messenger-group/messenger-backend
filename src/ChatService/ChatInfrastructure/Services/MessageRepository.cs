﻿using ChatApplication;
using ChatDomain.Entities;
using ChatInfrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace ChatInfrastructure.Services;

internal class MessageRepository : IMessageRepository
{
    private readonly ChatContext _chatContext;

    public MessageRepository(ChatContext chatContext)
    {
        _chatContext = chatContext;
    }

    public Task<List<Message>> GetOrderedMessages(Guid chatId, CancellationToken cancellationToken = default)
    {
        return _chatContext
            .Messages
            .Where(x => x.ChatId == chatId)
            .OrderByDescending(x => x.CreatedAt)
            .ToListAsync(cancellationToken);
    }

    public Task<List<Message>> GetMessages(IReadOnlyCollection<Guid> ids, CancellationToken cancellationToken = default)
    {
        return _chatContext
            .Messages
            .Where(x => ids.Contains(x.Id))
            .ToListAsync(cancellationToken);
    }

    public Task<Message?> GetById(Guid id, CancellationToken cancellationToken = default)
    {
        return _chatContext
            .Messages
            .Where(x => x.Id == id)
            .SingleOrDefaultAsync(cancellationToken);
    }

    public async Task CreateMessage(Message message, CancellationToken cancellationToken = default)
    {
        _chatContext
            .Messages
            .Add(message);

        await _chatContext.SaveChangesAsync(cancellationToken);
    }

    public async Task DeleteMessage(Message message, CancellationToken cancellationToken = default)
    {
        _chatContext
            .Messages
            .Remove(message);

        await _chatContext.SaveChangesAsync(cancellationToken);
    }

    public async Task SaveChanges(CancellationToken cancellationToken = default)
    {
        await _chatContext.SaveChangesAsync(cancellationToken);
    }
}