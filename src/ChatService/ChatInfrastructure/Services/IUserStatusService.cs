﻿namespace ChatInfrastructure.Services;

public record UserStatus(bool IsActive, DateTime LastStatusChangeDate);

public interface IUserStatusService
{
    Task<UserStatus> GetUserStatus(Guid userId, CancellationToken cancellationToken = default);

    Task SetUserStatus(Guid userId, bool isActive, CancellationToken cancellationToken = default);
}