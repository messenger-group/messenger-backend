﻿using ChatInfrastructure.Persistence;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using SharedEvents;

namespace ChatInfrastructure.Services;

internal class SendEmailNotificationAboutUnreadMessages : ISendEmailNotificationAboutUnreadMessages
{
    private const string MessageTitle = "Непрочитанные сообщения";
    private const string MessageFormat = "{0}! У вас есть непрочитанные сообщения";

    private readonly ChatContext _chatContext;
    private readonly IUserStatusService _userStatusService;
    private readonly IPublishEndpoint _publishEndpoint;

    public SendEmailNotificationAboutUnreadMessages(
        ChatContext chatContext,
        IPublishEndpoint publishEndpoint,
        IUserStatusService userStatusService)
    {
        _chatContext = chatContext;
        _publishEndpoint = publishEndpoint;
        _userStatusService = userStatusService;
    }

    public async Task SendNotification(CancellationToken cancellationToken = default)
    {
        await using var transaction = await _chatContext.Database.BeginTransactionAsync(cancellationToken);

        var readStatuses = await _chatContext
            .UserChatUnreadMessages
            .Select(x => new
            {
                Id = x.UserId,
                Name = x.User.FullName,
                x.User.Email,
                x.CreatedAt
            })
            .ToListAsync(cancellationToken);

        if (readStatuses.Count == 0)
        {
            return;
        }

        var usersToClearUnreadStatus = new List<Guid>();

        var now = DateTime.UtcNow;
        var offlineTimeSpan = TimeSpan.FromMinutes(5);

        foreach (var readStatus in readStatuses)
        {
            if (now.Subtract(readStatus.CreatedAt) <= offlineTimeSpan)
            {
                continue;
            }

            var userStatus = await _userStatusService.GetUserStatus(readStatus.Id, cancellationToken);
            if (userStatus.IsActive)
            {
                continue;
            }

            usersToClearUnreadStatus.Add(readStatus.Id);
            await _publishEndpoint.Publish(new SendEmailNotification(
                    readStatus.Email,
                    MessageTitle,
                    string.Format(MessageFormat, readStatus.Name)),
                cancellationToken);
        }

        await _chatContext
            .UserChatUnreadMessages
            .Where(x => usersToClearUnreadStatus.Contains(x.UserId))
            .ExecuteDeleteAsync(cancellationToken);

        await _chatContext.SaveChangesAsync(cancellationToken);

        await transaction.CommitAsync(cancellationToken);
    }
}