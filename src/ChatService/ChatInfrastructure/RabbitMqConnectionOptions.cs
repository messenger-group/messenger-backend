﻿using DataBindingsExtensions;

namespace ChatInfrastructure;

internal class RabbitMqConnectionOptions
{
    [EnvironmentVariableName("RABBIT_MQ_HOST")]
    public string Host { get; set; } = string.Empty;

    [EnvironmentVariableName("RABBIT_MQ_USERNAME")]
    public string UserName { get; set; } = string.Empty;

    [EnvironmentVariableName("RABBIT_MQ_PASSWORD")]
    public string Password { get; set; } = string.Empty;
}