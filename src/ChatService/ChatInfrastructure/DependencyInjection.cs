﻿using ChatApplication;
using ChatApplication.UseCases.Chats;
using ChatApplication.UseCases.Messages;
using ChatInfrastructure.Consumers;
using ChatInfrastructure.Persistence;
using ChatInfrastructure.Services;
using DataBindingsExtensions;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace ChatInfrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services)
    {
        return services
            .AddDataBase()
            .AddMasstransit()
            .AddChatHandlers()
            .AddMessageHandlers()
            .AddRepositories()
            .AddScoped<ISendEmailNotificationAboutUnreadMessages, SendEmailNotificationAboutUnreadMessages>();
    }

    public static async Task InitDb(this IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();

        var context = scope.ServiceProvider.GetRequiredService<ChatContext>();

        await context.Database.EnsureCreatedAsync();
    }

    private static IServiceCollection AddRepositories(this IServiceCollection services)
    {
        return services
            .AddScoped<IUsersRepository, UsersRepository>()
            .AddScoped<IChatRepository, ChatRepository>()
            .AddScoped<IMessageRepository, MessageRepository>();
    }

    private static IServiceCollection AddChatHandlers(this IServiceCollection services)
    {
        return services
            .AddScoped<GetOrCreateChatHandler>()
            .AddScoped<GetChatHandler>()
            .AddScoped<GetUserChatsHandler>()
            .AddScoped<GetChatByMessageHandler>();
    }

    private static IServiceCollection AddMessageHandlers(this IServiceCollection services)
    {
        return services
            .AddScoped<GetMessagesHandler>()
            .AddScoped<CreateMessageHandler>()
            .AddScoped<EditMessageHandler>()
            .AddScoped<DeleteMessageHandler>()
            .AddScoped<ReadMessagesHandler>();
    }

    private static IServiceCollection AddMasstransit(this IServiceCollection services)
    {
        var connectionOptions = GetConnectionOptions();

        return services
            .AddMassTransit(options =>
            {
                options.SetKebabCaseEndpointNameFormatter();

                options.AddConsumer<UserCreatedEventConsumer>();

                options.AddEntityFrameworkOutbox<ChatContext>(o =>
                {
                    o.DuplicateDetectionWindow = TimeSpan.FromMinutes(1);
                    o.QueryDelay = TimeSpan.FromSeconds(30);
                    o.UsePostgres();
                });

                options.UsingRabbitMq((context, config) =>
                {
                    config.Host(connectionOptions.Host, "/", h =>
                    {
                        h.Username(connectionOptions.UserName);
                        h.Password(connectionOptions.Password);
                    });

                    config.ConfigureEndpoints(context);
                });
            });

        static RabbitMqConnectionOptions GetConnectionOptions()
        {
            return EnvironmentLoader.Load<RabbitMqConnectionOptions>() ??
                   throw new Exception("RabbitMq options not found");
        }
    }

    private static IServiceCollection AddDataBase(this IServiceCollection services)
    {
        var connectionString = GetConnectionString();

        services.AddDbContext<ChatContext>(options => options.UseNpgsql(connectionString));

        return services;

        static string GetConnectionString()
        {
            return EnvironmentLoader.Load<ChatsDataBaseConnectionOptions>()?.ToString() ?? string.Empty;
        }
    }
}