﻿using ChatDomain.Entities;
using ChatInfrastructure.Persistence;
using MassTransit;
using SharedEvents;

namespace ChatInfrastructure.Consumers;

internal class UserCreatedEventConsumer : IConsumer<UserCreatedEvent>
{
    private readonly ChatContext _chatContext;

    public UserCreatedEventConsumer(ChatContext chatContext)
    {
        _chatContext = chatContext;
    }

    public async Task Consume(ConsumeContext<UserCreatedEvent> context)
    {
        var message = context.Message;

        _chatContext
            .Users
            .Add(new User
            {
                Id = message.Id,
                FullName = message.Patronymic is null
                    ? $"{message.Surname} {message.Name}"
                    : $"{message.Surname} {message.Name} {message.Patronymic}",
                Email = message.Email
            });

        await _chatContext.SaveChangesAsync();
    }
}