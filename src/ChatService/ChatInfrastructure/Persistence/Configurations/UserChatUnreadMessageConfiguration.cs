﻿using ChatDomain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ChatInfrastructure.Persistence.Configurations;

internal class UserChatUnreadMessageConfiguration : IEntityTypeConfiguration<UserChatUnreadMessage>
{
    public void Configure(EntityTypeBuilder<UserChatUnreadMessage> builder)
    {
        builder.HasKey(x => new { x.UserId, x.ChatId });

        builder
            .HasOne<Chat>()
            .WithMany(x => x.UserChatUnreadMessages)
            .HasForeignKey(x => x.ChatId)
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasOne(x => x.User)
            .WithMany()
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .Navigation(x => x.User)
            .AutoInclude();

        builder.HasIndex(x => x.ChatId);
        builder.HasIndex(x => x.UserId);
    }
}