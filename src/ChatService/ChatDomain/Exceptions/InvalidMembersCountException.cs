﻿namespace ChatDomain.Exceptions;

public class InvalidMembersCountException() : Exception("В чате должно быть не менее двух участников");