﻿namespace ChatDomain;

public enum ChatType
{
    Private,
    Group
}