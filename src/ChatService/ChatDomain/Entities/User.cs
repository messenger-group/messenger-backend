﻿namespace ChatDomain.Entities;

public class User
{
    public required Guid Id { get; init; }
    public required string FullName { get; init; }
    public required string Email { get; init; }
}