﻿namespace ChatDomain.Entities;

public class ChatMember
{
    public required Guid Id { get; init; }

    public required Guid UserId { get; init; }
    public required User User { get; init; }

    public required Guid ChatId { get; init; }
}