﻿using System.Diagnostics.CodeAnalysis;
using ChatDomain.Exceptions;

namespace ChatDomain.Entities;

public class Chat
{
    private readonly List<ChatMember> _members = [];
    private readonly List<UserChatUnreadMessage> _userChatUnreadMessages = [];

    public Guid Id { get; init; }
    public ChatType Type { get; init; }

    public IReadOnlyCollection<ChatMember> Members => _members;
    public IReadOnlyCollection<UserChatUnreadMessage> UserChatUnreadMessages => _userChatUnreadMessages;

    internal Chat()
    {
    }

    private Chat(Guid id, ChatType type, IEnumerable<ChatMember> members)
    {
        Id = id;
        Type = type;
        _members.AddRange(members);
    }

    public bool TryGetMember(Guid userId, [NotNullWhen(true)] out ChatMember? member)
    {
        member = _members.FirstOrDefault(x => x.UserId == userId);
        return member is not null;
    }

    public void ReadBy(ChatMember member)
    {
        _userChatUnreadMessages.RemoveAll(x => x.UserId == member.UserId);
    }

    public Message WriteMessageToChat(ChatMember sender, string text)
    {
        var now = DateTime.UtcNow;
        
        CreateUnreadStatusIfNotExists(sender, now);
        return new Message(Id, sender, text,now);
    }

    public static Chat CreateChat(ICollection<User> members)
    {
        if (members.Count < 2)
        {
            throw new InvalidMembersCountException();
        }

        var chatType = members.Count == 2 ? ChatType.Private : ChatType.Group;
        var chatId = Guid.NewGuid();

        return new Chat(chatId, chatType, members.Select(x => new ChatMember
        {
            Id = Guid.NewGuid(),
            UserId = x.Id,
            User = x,
            ChatId = chatId
        }));
    }

    private void CreateUnreadStatusIfNotExists(ChatMember sender, DateTime now)
    {
        foreach (var member in _members)
        {
            if (member == sender)
            {
                continue;
            }

            var memberReadStatus = _userChatUnreadMessages
                .FirstOrDefault(x => x.UserId == member.UserId);
            if (memberReadStatus is null)
            {
                _userChatUnreadMessages.Add(new UserChatUnreadMessage
                {
                    ChatId = Id,
                    UserId = member.UserId,
                    User = member.User,
                    CreatedAt = now
                });
            }
        }
    }
}