﻿namespace ChatDomain.Entities;

public class Message
{
    public Guid Id { get; init; }
    public Guid ChatId { get; init; }

    public string Text { get; set; }

    public Guid SenderId { get; init; }
    public ChatMember Sender { get; init; }
    public DateTime CreatedAt { get; init; }

    internal Message()
    {
    }

    internal Message(Guid chatId, ChatMember sender, string text, DateTime createdAt)
    {
        Id = Guid.NewGuid();
        ChatId = chatId;

        Text = text;

        SenderId = sender.Id;
        Sender = sender;
        CreatedAt = createdAt;
    }

    public bool IsCreatedBy(Guid userId)
    {
        return Sender.UserId == userId;
    }
}