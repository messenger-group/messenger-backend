﻿namespace ChatDomain.Entities;

public class UserChatUnreadMessage
{
    public required Guid ChatId { get; init; }

    public required Guid UserId { get; init; }
    public required User User { get; init; }

    public required DateTime CreatedAt { get; set; }
}