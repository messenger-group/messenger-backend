﻿using ChatInfrastructure.Services;
using Quartz;

namespace ChatWebApi.Jobs;

public class SendEmailNotificationAboutUnreadMessagesJob : IJob
{
    private readonly ISendEmailNotificationAboutUnreadMessages _notification;

    public SendEmailNotificationAboutUnreadMessagesJob(ISendEmailNotificationAboutUnreadMessages notification)
    {
        _notification = notification;
    }

    public Task Execute(IJobExecutionContext context)
    {
        return _notification.SendNotification();
    }
}