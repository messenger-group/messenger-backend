﻿using ChatInfrastructure.Services;
using ChatWebApi.Jobs;
using ChatWebApi.Services;
using Quartz;

namespace ChatWebApi;

public static class DependencyInjection
{
    public static IServiceCollection AddWebApi(this IServiceCollection services)
    {
        var redisHost = Environment.GetEnvironmentVariable("REDIS_HOST") ?? string.Empty;
        var redisPort = Environment.GetEnvironmentVariable("REDIS_PORT") ?? string.Empty;

        services
            .AddStackExchangeRedisCache(options =>
            {
                options.Configuration = $"{redisHost}:{redisPort}";
                options.InstanceName = "ChatWebApi";
            })
            .AddScoped<IUserStatusService, UserStatusService>()
            .AddScoped<IUserIdService, UserIdFromHeaderService>()
            .AddSignalR(options => options.EnableDetailedErrors = true);

        services
            .AddQuartz(config =>
            {
                var jobKey = new JobKey(nameof(SendEmailNotificationAboutUnreadMessagesJob));
                config.AddJob<SendEmailNotificationAboutUnreadMessagesJob>(
                    options => options.WithIdentity(jobKey));

                config.AddTrigger(options => options
                    .ForJob(jobKey)
                    .WithIdentity($"{nameof(SendEmailNotificationAboutUnreadMessagesJob)}-trigger")
                    .WithCronSchedule("0 * * ? * * *")
                );
            })
            .AddQuartzHostedService(options => options.WaitForJobsToComplete = true);

        return services;
    }
}