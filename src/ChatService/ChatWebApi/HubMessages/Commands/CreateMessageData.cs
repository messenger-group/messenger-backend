﻿namespace ChatWebApi.HubMessages.Commands;

public record CreateMessageData(Guid ChatId, string Text);