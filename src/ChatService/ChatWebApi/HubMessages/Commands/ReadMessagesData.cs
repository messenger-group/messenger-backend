﻿namespace ChatWebApi.HubMessages.Commands;

public record ReadMessagesData(Guid ChatId);