﻿namespace ChatWebApi.HubMessages.Commands;

public record DeleteMessageData(Guid MessageId);