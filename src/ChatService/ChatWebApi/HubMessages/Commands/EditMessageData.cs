﻿namespace ChatWebApi.HubMessages.Commands;

public record EditMessageData(Guid MessageId, string Text);