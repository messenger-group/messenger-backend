﻿namespace ChatWebApi.HubMessages.Events;

public record MessageDeletedData(Guid MessageId, Guid ChatId);