﻿namespace ChatWebApi.HubMessages.Events;

public record MessageEditedData(Guid MessageId, Guid ChatId, string Text);