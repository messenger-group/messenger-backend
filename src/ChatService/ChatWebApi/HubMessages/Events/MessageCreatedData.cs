﻿using ChatApplication.Dto;

namespace ChatWebApi.HubMessages.Events;

public record MessageCreatedData(Guid MessageId, Guid ChatId, string Text, MemberDto Sender);