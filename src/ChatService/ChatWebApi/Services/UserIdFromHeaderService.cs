﻿namespace ChatWebApi.Services;

public class UserIdFromHeaderService : IUserIdService
{
    private const string UserIdHeader = "User-Id";

    private readonly IHttpContextAccessor _httpContextAccessor;

    public UserIdFromHeaderService(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    public Guid? CurrentUserId => GetCurrentUserId();

    private Guid? GetCurrentUserId()
    {
        var httpContext = _httpContextAccessor.HttpContext;
        if (httpContext is null)
        {
            return null;
        }

        if (httpContext.Request.Headers.TryGetValue(UserIdHeader, out var header)
            && Guid.TryParse(header, out var id))
        {
            return id;
        }

        return null;
    }
}