﻿using System.Text.Json;
using ChatInfrastructure.Services;
using Microsoft.Extensions.Caching.Distributed;

namespace ChatWebApi.Services;

public class UserStatusService : IUserStatusService
{
    private readonly IDistributedCache _distributedCache;

    public UserStatusService(IDistributedCache distributedCache)
    {
        _distributedCache = distributedCache;
    }

    public async Task<UserStatus> GetUserStatus(Guid userId, CancellationToken cancellationToken = default)
    {
        var status = await _distributedCache.GetAsync(CreateKey(userId), cancellationToken);
        return status is null
            ? new UserStatus(false, DateTime.MinValue)
            : Restore(status);
    }

    public Task SetUserStatus(Guid userId, bool isActive, CancellationToken cancellationToken = default)
    {
        return _distributedCache.SetAsync(
            CreateKey(userId),
            Serialize(new UserStatus(isActive, DateTime.UtcNow)),
            cancellationToken);
    }

    private static string CreateKey(Guid userId)
    {
        return $"user_status_{userId}";
    }

    private static byte[] Serialize(UserStatus status)
    {
        return JsonSerializer.SerializeToUtf8Bytes(status);
    }

    private static UserStatus Restore(byte[] serializedStatus)
    {
        return JsonSerializer.Deserialize<UserStatus>(serializedStatus)!;
    }
}