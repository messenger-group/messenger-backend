﻿namespace ChatWebApi.Services;

public interface IUserIdService
{
    Guid? CurrentUserId { get; }
}