﻿using ChatApplication.UseCases.Chats;
using Microsoft.AspNetCore.Mvc;

namespace ChatWebApi;

public static class ChatEndpoints
{
    public static void MapChatEndpoints(this WebApplication app)
    {
        var group = app
            .MapGroup("api/chat")
            .WithOpenApi()
            .WithTags("Chat");

        group.MapPost(string.Empty, async (
                [FromBody] GetOrCreateChat command,
                [FromServices] GetOrCreateChatHandler handler,
                CancellationToken cancellationToken)
            => Results.Ok(await handler.Handle(command, cancellationToken)));
    }
}