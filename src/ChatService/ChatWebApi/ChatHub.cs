﻿using ChatApplication.Dto;
using ChatApplication.UseCases.Chats;
using ChatApplication.UseCases.Messages;
using ChatInfrastructure.Services;
using ChatWebApi.HubMessages.Commands;
using ChatWebApi.HubMessages.Events;
using ChatWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace ChatWebApi;

public class ChatHub : Hub
{
    private static class Messages
    {
        public const string MessageSent = nameof(MessageSent);
        public const string MessageEdited = nameof(MessageEdited);
        public const string MessageDeleted = nameof(MessageDeleted);
    }

    private readonly IUserIdService _userIdService;
    private readonly IUserStatusService _userStatusService; 

    public ChatHub(IUserIdService userIdService, IUserStatusService userStatusService)
    {
        _userIdService = userIdService;
        _userStatusService = userStatusService;
    }

    public override async Task OnConnectedAsync()
    {
        var userId = _userIdService.CurrentUserId;
        if (userId is null)
        {
            Context.Abort();
            return;
        }

        await _userStatusService.SetUserStatus(userId.Value, true);

        await Groups.AddToGroupAsync(Context.ConnectionId, CreateGroupFromUserId(userId.Value));
    }

    public override async Task OnDisconnectedAsync(Exception? exception)
    {
        await _userStatusService.SetUserStatus(GetUserId(), false);
    }

    public async Task SendMessage(
        CreateMessageData data,
        [FromServices] GetChatHandler getChatHandler,
        [FromServices] CreateMessageHandler createMessageHandler,
        [FromServices] CancellationToken cancellationToken)
    {
        var message = await createMessageHandler
            .Handle(new CreateMessage(GetUserId(), data.ChatId, data.Text), cancellationToken);

        var sendTo = await GetSendTo(data.ChatId, getChatHandler, cancellationToken);

        await Clients
            .Groups(sendTo)
            .SendAsync(
                Messages.MessageSent,
                new MessageCreatedData(message.Id, message.ChatId, message.Text, message.Sender),
                cancellationToken);
    }

    public async Task EditMessage(
        EditMessageData data,
        [FromServices] GetChatHandler getChatHandler,
        [FromServices] EditMessageHandler editMessageHandler,
        [FromServices] CancellationToken cancellationToken)
    {
        var message = await editMessageHandler
            .Handle(new EditMessage(GetUserId(), data.MessageId, data.Text), cancellationToken);

        var sendTo = await GetSendTo(message.ChatId, getChatHandler, cancellationToken);

        await Clients
            .Groups(sendTo)
            .SendAsync(
                Messages.MessageEdited,
                new MessageEditedData(message.Id, message.ChatId, message.Text),
                cancellationToken);
    }

    public async Task<List<ChatDto>> GetChats(
        [FromServices] GetUserChatsHandler getUserChatsHandler,
        [FromServices] CancellationToken cancellationToken)
    {
        return await getUserChatsHandler.Handle(new GetUserChats(GetUserId()), cancellationToken);
    }

    public async Task<List<MessageDto>> GetChatMessages(
        Guid chatId,
        [FromServices] GetMessagesHandler getMessages,
        [FromServices] CancellationToken cancellationToken)
    {
        return await getMessages.Handle(new GetMessages(GetUserId(), chatId), cancellationToken);
    }

    public async Task DeleteMessage(
        DeleteMessageData data,
        [FromServices] GetChatByMessageHandler getChatHandler,
        [FromServices] DeleteMessageHandler deleteMessageHandler,
        [FromServices] CancellationToken cancellationToken)
    {
        var chat = await getChatHandler.Handle(new GetChatByMessage(data.MessageId), cancellationToken);

        await deleteMessageHandler
            .Handle(new DeleteMessage(GetUserId(), data.MessageId), cancellationToken);

        var sendTo = GetSendTo(chat);

        await Clients
            .Groups(sendTo)
            .SendAsync(
                Messages.MessageDeleted,
                new MessageDeletedData(data.MessageId, chat.Id),
                cancellationToken);
    }

    public async Task ReadMessages(
        ReadMessagesData data,
        [FromServices] ReadMessagesHandler readMessagesHandler,
        [FromServices] CancellationToken cancellationToken)
    {
        await readMessagesHandler.Handle(
            new ReadMessages(GetUserId(), data.ChatId),
            cancellationToken);
    }

    private Guid GetUserId()
    {
        return _userIdService.CurrentUserId!.Value;
    }

    private static async Task<List<string>> GetSendTo(
        Guid chatId,
        GetChatHandler getChatHandler,
        CancellationToken cancellationToken)
    {
        var chat = await getChatHandler.Handle(new GetChat(chatId), cancellationToken);

        return GetSendTo(chat);
    }

    private static List<string> GetSendTo(ChatDto chat)
    {
        return chat
            .Members
            .Select(x => CreateGroupFromUserId(x.UserId))
            .ToList();
    }

    private static string CreateGroupFromUserId(Guid userId)
    {
        return $"user_{userId}";
    }
}