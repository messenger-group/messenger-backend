using ChatInfrastructure;
using ChatWebApi;

var builder = WebApplication.CreateBuilder(args);

var services = builder.Services;

services
    .AddHttpContextAccessor()
    .AddEndpointsApiExplorer()
    .AddSwaggerGen()
    .AddInfrastructure()
    .AddWebApi();

var app = builder.Build();

await app.Services.InitDb();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapChatEndpoints();
app.MapHub<ChatHub>("hub/chat");

app.Run();