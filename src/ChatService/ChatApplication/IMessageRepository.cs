﻿using ChatDomain.Entities;

namespace ChatApplication;

public interface IMessageRepository
{
    Task<List<Message>> GetOrderedMessages(Guid chatId, CancellationToken cancellationToken = default);
    Task<List<Message>> GetMessages(IReadOnlyCollection<Guid> ids, CancellationToken cancellationToken = default);
    Task<Message?> GetById(Guid id, CancellationToken cancellationToken = default);

    Task CreateMessage(Message message, CancellationToken cancellationToken = default);
    Task DeleteMessage(Message message, CancellationToken cancellationToken = default);

    Task SaveChanges(CancellationToken cancellationToken = default);
}