﻿namespace ChatApplication.Dto;

public record ChatDto(Guid Id, List<MemberDto> Members);