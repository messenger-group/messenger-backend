﻿namespace ChatApplication.Dto;

public record MemberDto(Guid UserId, string FullName);