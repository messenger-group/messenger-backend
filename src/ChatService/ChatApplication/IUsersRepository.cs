﻿using ChatDomain.Entities;

namespace ChatApplication;

public interface IUsersRepository
{
    Task<List<User>> GetUsers(IReadOnlyCollection<Guid> userIds, CancellationToken cancellationToken = default);
}