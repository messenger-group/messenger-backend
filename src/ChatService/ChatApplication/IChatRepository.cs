﻿using ChatDomain.Entities;

namespace ChatApplication;

public interface IChatRepository
{

    IAsyncEnumerable<Chat> GetUserChats(Guid userId);
    Task<Chat?> GetById(Guid chatId, CancellationToken cancellationToken = default);

    Task<Chat?> TryGetExistingPrivateChat(
        IReadOnlyCollection<Guid> userIds,
        CancellationToken cancellationToken = default);

    Task SaveChat(Chat chat, CancellationToken cancellationToken = default);

    Task UpdateChat(Chat chat, CancellationToken cancellationToken = default);
}