﻿namespace ChatApplication.Exceptions;

public class ChatNotFoundException() : Exception("Чат не найден");