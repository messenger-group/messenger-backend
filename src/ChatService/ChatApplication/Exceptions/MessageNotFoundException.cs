﻿namespace ChatApplication.Exceptions;

public class MessageNotFoundException() : Exception("Сообщение не найдено");