﻿namespace ChatApplication.Exceptions;

public class UnableToEditMessageException() : Exception("Вы не можете редактировать чужие сообщения");