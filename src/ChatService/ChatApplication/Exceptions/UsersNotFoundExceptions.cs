﻿namespace ChatApplication.Exceptions;

public class UsersNotFoundExceptions() : Exception("Пользователи не найдены");