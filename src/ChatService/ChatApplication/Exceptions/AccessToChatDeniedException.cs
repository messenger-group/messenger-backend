﻿namespace ChatApplication.Exceptions;

public class AccessToChatDeniedException() : Exception("Вы не участиник чата");