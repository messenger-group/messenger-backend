﻿using ChatApplication.Exceptions;

namespace ChatApplication.UseCases.Messages;

public record ReadMessages(Guid UserId, Guid ChatId);

public class ReadMessagesHandler
{
    private readonly IChatRepository _chatRepository;

    public ReadMessagesHandler(IChatRepository chatRepository)
    {
        _chatRepository = chatRepository;
    }

    public async Task Handle(ReadMessages command, CancellationToken cancellationToken)
    {
        var chat = await _chatRepository.GetById(command.ChatId, cancellationToken)
                   ?? throw new ChatNotFoundException();

        if (chat.TryGetMember(command.UserId, out var member) is false)
        {
            throw new AccessToChatDeniedException();
        }

        chat.ReadBy(member);

        await _chatRepository.UpdateChat(chat, cancellationToken);
    }
}