﻿using ChatApplication.Dto;
using ChatApplication.Exceptions;

namespace ChatApplication.UseCases.Messages;

public record CreateMessage(Guid UserId, Guid ChatId, string Text);

public class CreateMessageHandler
{
    private readonly IChatRepository _chatRepository;
    private readonly IMessageRepository _messageRepository;

    public CreateMessageHandler(IChatRepository chatRepository, IMessageRepository messageRepository)
    {
        _chatRepository = chatRepository;
        _messageRepository = messageRepository;
    }

    public async Task<MessageDto> Handle(CreateMessage command, CancellationToken cancellationToken)
    {
        var chat = await _chatRepository.GetById(command.ChatId, cancellationToken)
                   ?? throw new ChatNotFoundException();

        if (chat.TryGetMember(command.UserId, out var sender) is false)
        {
            throw new AccessToChatDeniedException();
        }

        var message = chat.WriteMessageToChat(sender, command.Text);

        await _messageRepository.CreateMessage(message, cancellationToken);

        return new MessageDto(
            message.Id,
            message.ChatId,
            message.Text,
            message.CreatedAt,
            new MemberDto(sender.UserId, sender.User.FullName));
    }
}