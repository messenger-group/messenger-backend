﻿using ChatApplication.Exceptions;

namespace ChatApplication.UseCases.Messages;

public record DeleteMessage(Guid UserId, Guid MessageId);

public class DeleteMessageHandler
{
    private readonly IMessageRepository _messageRepository;

    public DeleteMessageHandler(IMessageRepository messageRepository)
    {
        _messageRepository = messageRepository;
    }

    public async Task Handle(DeleteMessage command, CancellationToken cancellationToken)
    {
        var message = await _messageRepository.GetById(command.MessageId, cancellationToken)
                      ?? throw new MessageNotFoundException();

        if (message.IsCreatedBy(command.UserId) is false)
        {
            throw new AccessToChatDeniedException();
        }

        await _messageRepository.DeleteMessage(message, cancellationToken);
    }
}