﻿using ChatApplication.Dto;
using ChatApplication.Exceptions;

namespace ChatApplication.UseCases.Messages;

public record EditMessage(Guid UserId, Guid MessageId, string Text);

public class EditMessageHandler
{
    private readonly IMessageRepository _messageRepository;

    public EditMessageHandler(IMessageRepository messageRepository)
    {
        _messageRepository = messageRepository;
    }

    public async Task<MessageDto> Handle(EditMessage command, CancellationToken cancellationToken)
    {
        var message = await _messageRepository.GetById(command.MessageId, cancellationToken)
                      ?? throw new MessageNotFoundException();

        if (message.IsCreatedBy(command.UserId) is false)
        {
            throw new UnableToEditMessageException();
        }

        message.Text = command.Text;

        await _messageRepository.SaveChanges(cancellationToken);

        var sender = message.Sender;
        return new MessageDto(
            message.Id,
            message.ChatId,
            message.Text,
            message.CreatedAt,
            new MemberDto(sender.UserId, sender.User.FullName));
    }
}