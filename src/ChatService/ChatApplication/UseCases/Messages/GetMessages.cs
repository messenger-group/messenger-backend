﻿using ChatApplication.Dto;
using ChatApplication.Exceptions;

namespace ChatApplication.UseCases.Messages;

public record GetMessages(Guid UserId, Guid ChatId);

public class GetMessagesHandler
{
    private readonly IMessageRepository _messageRepository;
    private readonly IChatRepository _chatRepository;

    public GetMessagesHandler(IMessageRepository messageRepository, IChatRepository chatRepository)
    {
        _messageRepository = messageRepository;
        _chatRepository = chatRepository;
    }

    public async Task<List<MessageDto>> Handle(GetMessages query, CancellationToken cancellationToken)
    {
        var chat = await _chatRepository.GetById(query.ChatId, cancellationToken)
                   ?? throw new ChatNotFoundException();

        if (chat.TryGetMember(query.UserId, out _) is false)
        {
            throw new AccessToChatDeniedException();
        }

        return (await _messageRepository.GetOrderedMessages(chat.Id, cancellationToken))
            .Select(x => new MessageDto(
                x.Id,
                x.ChatId,
                x.Text,
                x.CreatedAt,
                new MemberDto(x.Sender.UserId, x.Sender.User.FullName)))
            .ToList();
    }
}