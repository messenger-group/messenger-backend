﻿using ChatApplication.Exceptions;
using ChatDomain.Entities;

namespace ChatApplication.UseCases.Chats;

public record GetOrCreateChat(HashSet<Guid> Members);

public class GetOrCreateChatHandler
{
    private readonly IChatRepository _chatRepository;
    private readonly IUsersRepository _usersRepository;

    public GetOrCreateChatHandler(IChatRepository chatRepository, IUsersRepository usersRepository)
    {
        _chatRepository = chatRepository;
        _usersRepository = usersRepository;
    }

    public async Task<Guid> Handle(GetOrCreateChat command, CancellationToken cancellationToken)
    {
        var existingPrivateChat = await _chatRepository.TryGetExistingPrivateChat(command.Members, cancellationToken);

        if (existingPrivateChat is not null)
        {
            return existingPrivateChat.Id;
        }

        var users = await _usersRepository.GetUsers(command.Members, cancellationToken);
        if (command.Members.Count != users.Count)
        {
            throw new UsersNotFoundExceptions();
        }

        var chat = Chat.CreateChat(users);

        await _chatRepository.SaveChat(chat, cancellationToken);

        return chat.Id;
    }
}