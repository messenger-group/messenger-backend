﻿using ChatApplication.Dto;

namespace ChatApplication.UseCases.Chats;

public record GetUserChats(Guid UserId);

public class GetUserChatsHandler
{
    private readonly IChatRepository _chatRepository;

    public GetUserChatsHandler(IChatRepository chatRepository)
    {
        _chatRepository = chatRepository;
    }

    public async Task<List<ChatDto>> Handle(GetUserChats query, CancellationToken cancellationToken)
    {
        var chats = new List<ChatDto>();

        await foreach (var chat in _chatRepository.GetUserChats(query.UserId).WithCancellation(cancellationToken))
        {
            chats.Add(new ChatDto(
                chat.Id,
                chat
                    .Members
                    .Select(x => new MemberDto(x.UserId, x.User.FullName))
                    .ToList()));
        }

        return chats;
    }
}