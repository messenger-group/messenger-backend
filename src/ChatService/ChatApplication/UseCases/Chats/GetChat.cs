﻿using ChatApplication.Dto;
using ChatApplication.Exceptions;

namespace ChatApplication.UseCases.Chats;

public record GetChat(Guid ChatId);

public class GetChatHandler
{
    private readonly IChatRepository _chatRepository;

    public GetChatHandler(IChatRepository chatRepository)
    {
        _chatRepository = chatRepository;
    }

    public async Task<ChatDto> Handle(GetChat query, CancellationToken cancellationToken)
    {
        var chat = await _chatRepository.GetById(query.ChatId, cancellationToken)
                   ?? throw new ChatNotFoundException();

        return new ChatDto(
            chat.Id,
            chat
                .Members
                .Select(x => new MemberDto(x.UserId, x.User.FullName))
                .ToList());
    }
}