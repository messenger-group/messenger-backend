﻿using ChatApplication.Dto;
using ChatApplication.Exceptions;

namespace ChatApplication.UseCases.Chats;

public record GetChatByMessage(Guid MessageId);

public class GetChatByMessageHandler
{
    private readonly IMessageRepository _messageRepository;
    private readonly GetChatHandler _getChatHandler;

    public GetChatByMessageHandler(IMessageRepository messageRepository, GetChatHandler getChatHandler)
    {
        _messageRepository = messageRepository;
        _getChatHandler = getChatHandler;
    }

    public async Task<ChatDto> Handle(GetChatByMessage query, CancellationToken cancellationToken = default)
    {
        var message = await _messageRepository.GetById(query.MessageId, cancellationToken)
                      ?? throw new MessageNotFoundException();

        return await _getChatHandler.Handle(new GetChat(message.ChatId), cancellationToken);
    }
}