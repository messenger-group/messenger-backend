﻿using System.Linq.Expressions;
using System.Reflection;

namespace DataBindingsExtensions;

public static class EnvironmentLoader
{
    public static T? Load<T>() where T : class
    {
        var type = typeof(T);

        var properties = type
            .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty);

        var inits = new List<MemberAssignment>(properties.Length);
        foreach (var property in properties)
        {
            var variableNameAttribute = property.GetCustomAttribute<EnvironmentVariableNameAttribute>();
            if (variableNameAttribute is null)
            {
                return null;
            }

            var variable = Environment.GetEnvironmentVariable(variableNameAttribute.Name);
            if (variable is null)
            {
                return null;
            }

            inits.Add(Expression.Bind(
                property,
                FromString(variable, property.PropertyType)));
        }

        var newExpression = Expression.New(type);
        var memberInit = Expression.MemberInit(newExpression, inits);
        var factory = Expression.Lambda<Func<T>>(memberInit).Compile();

        return factory();
    }

    private static ConstantExpression FromString(string value, Type toType)
    {
        if (toType == typeof(string))
        {
            return Expression.Constant(value, toType);
        }

        if (toType == typeof(int))
        {
            return Expression.Constant(int.Parse(value), toType);
        }

        if (toType == typeof(long))
        {
            return Expression.Constant(long.Parse(value), toType);
        }

        if (toType == typeof(short))
        {
            return Expression.Constant(short.Parse(value), toType);
        }

        if (toType == typeof(bool))
        {
            return Expression.Constant(bool.Parse(value), toType);
        }

        if (toType.IsEnum)
        {
            return Expression.Constant(Enum.Parse(toType, value), toType);
        }

        throw new Exception("Unsupported environment variable type");
    }
}