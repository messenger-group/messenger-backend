﻿namespace DataBindingsExtensions;

[AttributeUsage(AttributeTargets.Property)]
public class EnvironmentVariableNameAttribute : Attribute
{
    public string Name { get; }

    public EnvironmentVariableNameAttribute(string name)
    {
        Name = name;
    }
}