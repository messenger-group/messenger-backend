using EmailInfrastructure;
using EmailWebApp.Jobs;
using Quartz;

var builder = WebApplication.CreateBuilder(args);

var services = builder.Services;

services.AddInfrastructure();

services
    .AddQuartz(config =>
    {
        var jobKey = new JobKey(nameof(SendUnhandledEmailsJob));
        config.AddJob<SendUnhandledEmailsJob>(options => options.WithIdentity(jobKey));

        config.AddTrigger(options => options
            .ForJob(jobKey)
            .WithIdentity($"{nameof(SendUnhandledEmailsJob)}-trigger")
            .WithCronSchedule("0 * * ? * * *")
        );
    })
    .AddQuartzHostedService(options => options.WaitForJobsToComplete = true);

var app = builder.Build();

await app.Services.InitDb();

app.Run();