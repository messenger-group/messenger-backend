﻿using EmailInfrastructure.Services;
using Quartz;

namespace EmailWebApp.Jobs;

public class SendUnhandledEmailsJob : IJob
{
    private readonly ISenderService _senderService;
    private readonly ILogger<SendUnhandledEmailsJob> _logger;

    public SendUnhandledEmailsJob(ISenderService senderService, ILogger<SendUnhandledEmailsJob> logger)
    {
        _senderService = senderService;
        _logger = logger;
    }

    public Task Execute(IJobExecutionContext context)
    {
        _logger.LogInformation("Job started");
        return _senderService.SendUnhandledMessages();
    }
}