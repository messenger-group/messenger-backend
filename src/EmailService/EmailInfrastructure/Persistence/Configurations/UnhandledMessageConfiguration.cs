﻿using EmailInfrastructure.Persistence.Entities;

namespace EmailInfrastructure.Persistence.Configurations;

internal class UnhandledMessageConfiguration : MessageConfiguration<UnhandledMessage>;