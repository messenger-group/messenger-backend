﻿using EmailInfrastructure.Persistence.Entities;

namespace EmailInfrastructure.Persistence.Configurations;

internal class HandledMessageConfiguration : MessageConfiguration<HandledMessage>;