﻿using EmailInfrastructure.Persistence.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmailInfrastructure.Persistence.Configurations;

internal abstract class MessageConfiguration<TMessage> : IEntityTypeConfiguration<TMessage>
    where TMessage : Message
{
    public virtual void Configure(EntityTypeBuilder<TMessage> builder)
    {
        builder.HasKey(x => x.Id);
        builder
            .Property(x => x.Id)
            .ValueGeneratedNever();
    }
}