﻿using EmailInfrastructure.Persistence.Configurations;
using EmailInfrastructure.Persistence.Entities;
using MassTransit;
using Microsoft.EntityFrameworkCore;

namespace EmailInfrastructure.Persistence;

internal class EmailContext : DbContext
{
    public DbSet<HandledMessage> HandledMessages => Set<HandledMessage>();
    public DbSet<UnhandledMessage> UnhandledMessages => Set<UnhandledMessage>();

    public EmailContext(DbContextOptions<EmailContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ApplyConfiguration(new HandledMessageConfiguration());
        modelBuilder.ApplyConfiguration(new UnhandledMessageConfiguration());

        modelBuilder.AddInboxStateEntity();
        modelBuilder.AddOutboxMessageEntity();
        modelBuilder.AddOutboxStateEntity();
    }
}