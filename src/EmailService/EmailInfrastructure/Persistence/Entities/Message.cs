﻿namespace EmailInfrastructure.Persistence.Entities;

public abstract class Message
{
    public required Guid Id { get; init; }
    public required string SentTo { get; init; }
    public required string Title { get; init; }
    public required string Text { get; init; }
}