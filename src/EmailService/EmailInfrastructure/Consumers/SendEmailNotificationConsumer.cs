﻿using EmailInfrastructure.Persistence;
using EmailInfrastructure.Persistence.Entities;
using MassTransit;
using SharedEvents;

namespace EmailInfrastructure.Consumers;

internal class SendEmailNotificationConsumer : IConsumer<SendEmailNotification>
{
    private readonly EmailContext _emailContext;

    public SendEmailNotificationConsumer(EmailContext emailContext)
    {
        _emailContext = emailContext;
    }

    public async Task Consume(ConsumeContext<SendEmailNotification> context)
    {
        var message = context.Message;

        _emailContext
            .UnhandledMessages
            .Add(new UnhandledMessage
            {
                Id = Guid.NewGuid(),
                SentTo = message.Email,
                Title = message.Title,
                Text = message.Text
            });

        await _emailContext.SaveChangesAsync();
    }
}