﻿using DataBindingsExtensions;

namespace EmailInfrastructure;

internal class SmtpServerOptions
{
    [EnvironmentVariableName("SMTP_HOST")]
    public required string Host { get; init; }

    [EnvironmentVariableName("SMTP_PORT")]
    public required int Port { get; init; }

    [EnvironmentVariableName("SMTP_USE_SSL")]
    public required bool UseSsl { get; init; }

    [EnvironmentVariableName("SMTP_SENDER_ADDRESS")]
    public required string SenderAddress { get; init; }

    [EnvironmentVariableName("SMTP_SENDER_NAME")]
    public required string SenderName { get; init; }
}