﻿using DataBindingsExtensions;
using EmailInfrastructure.Consumers;
using EmailInfrastructure.Persistence;
using EmailInfrastructure.Services;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace EmailInfrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services)
    {
        return services
            .AddDataBase()
            .AddMasstransit()
            .AddSmtpOptions()
            .AddScoped<ISenderService, SenderService>();
    }

    public static async Task InitDb(this IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();

        var context = scope.ServiceProvider.GetRequiredService<EmailContext>();

        await context.Database.EnsureCreatedAsync();
    }

    private static IServiceCollection AddMasstransit(this IServiceCollection services)
    {
        var connectionOptions = GetConnectionOptions();

        return services
            .AddMassTransit(options =>
            {
                options.SetKebabCaseEndpointNameFormatter();

                options.AddConsumer<SendEmailNotificationConsumer>();

                options.AddEntityFrameworkOutbox<EmailContext>(o =>
                {
                    o.DuplicateDetectionWindow = TimeSpan.FromMinutes(1);
                    o.QueryDelay = TimeSpan.FromSeconds(30);
                    o.UsePostgres();
                });

                options.UsingRabbitMq((context, config) =>
                {
                    config.Host(connectionOptions.Host, "/", h =>
                    {
                        h.Username(connectionOptions.UserName);
                        h.Password(connectionOptions.Password);
                    });

                    config.ConfigureEndpoints(context);
                });
            });

        static RabbitMqConnectionOptions GetConnectionOptions()
        {
            return EnvironmentLoader.Load<RabbitMqConnectionOptions>() ??
                   throw new Exception("RabbitMq options not found");
        }
    }

    private static IServiceCollection AddSmtpOptions(this IServiceCollection services)
    {
        var smtpOptions = GetSmtpOptions();

        return services.AddSingleton(smtpOptions);

        static SmtpServerOptions GetSmtpOptions()
        {
            return EnvironmentLoader.Load<SmtpServerOptions>() ??
                   throw new Exception("Smtp options not found");
        }
    }

    private static IServiceCollection AddDataBase(this IServiceCollection services)
    {
        var connectionString = GetConnectionString();

        services.AddDbContext<EmailContext>(options => options.UseNpgsql(connectionString));

        return services;

        static string GetConnectionString()
        {
            return EnvironmentLoader.Load<EmailDataBaseConnectionOptions>()?.ToString() ?? string.Empty;
        }
    }
}