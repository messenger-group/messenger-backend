﻿using EmailInfrastructure.Persistence;
using EmailInfrastructure.Persistence.Entities;
using MailKit.Net.Smtp;
using Microsoft.EntityFrameworkCore;
using MimeKit;

namespace EmailInfrastructure.Services;

internal class SenderService : ISenderService
{
    private readonly EmailContext _emailContext;
    private readonly SmtpServerOptions _smtpOptions;

    public SenderService(EmailContext emailContext, SmtpServerOptions smtpOptions)
    {
        _emailContext = emailContext;
        _smtpOptions = smtpOptions;
    }

    public async Task SendUnhandledMessages(CancellationToken cancellationToken = default)
    {
        var unhandledMessages = await _emailContext
            .UnhandledMessages
            .ToListAsync(cancellationToken);

        if (unhandledMessages.Count == 0)
        {
            return;
        }

        using var smtp = new SmtpClient();

        await smtp.ConnectAsync(_smtpOptions.Host, _smtpOptions.Port, _smtpOptions.UseSsl, cancellationToken);

        foreach (var unhandledMessage in unhandledMessages)
        {
            var email = new MimeMessage();

            email.From.Add(new MailboxAddress(_smtpOptions.SenderName, _smtpOptions.SenderAddress));
            email.To.Add(new MailboxAddress(unhandledMessage.SentTo, unhandledMessage.SentTo));

            email.Subject = unhandledMessage.Title;
            email.Body = new TextPart(MimeKit.Text.TextFormat.Text)
            {
                Text = unhandledMessage.Text
            };

            await smtp.SendAsync(email, cancellationToken);
        }

        _emailContext
            .UnhandledMessages
            .RemoveRange(unhandledMessages);

        _emailContext
            .HandledMessages
            .AddRange(unhandledMessages
                .Select(x => new HandledMessage
                {
                    Id = Guid.NewGuid(),
                    SentTo = x.SentTo,
                    Title = x.Title,
                    Text = x.Text
                }));

        await _emailContext.SaveChangesAsync(cancellationToken);
    }
}