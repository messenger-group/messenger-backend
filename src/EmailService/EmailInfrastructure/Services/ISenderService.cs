﻿namespace EmailInfrastructure.Services;

public interface ISenderService
{
    Task SendUnhandledMessages(CancellationToken cancellationToken = default);
}